![Bild 1](media/1.jpeg)

**Kraven i Nya Karolinska Solna-projektet är tydliga – all teknisk dokumentation ska, direkt eller indirekt, vara kopplad till en objektorienterad modell.**

# BIM säkrar kvalitet och tid i Nya Karolinska-projektet 

> ##### Nya Karolinska Solna blir Skanskas och Sveriges största BIM-projekt. BIM ska säkra kvaliteten och tidsplanen i projektet och skapa ett effektivt utnyttjande av resurserna. En kärngrupp inom Skanska ska se till att BIM genomsyrar i stort sett allt i projektet och att beställarkraven på BIM uppfylls.

– ETT PROJEKT I DEN HÄR STORLEKSORDNINGEN förutsätter BIM för att kunna genomföras, menar John Fahlgren, BIM-koordinator inom Skanska Healthcare AB. I Nya Karolinska Solna-projektet är det en liten kärngrupp
​	– fyra personer på heltid, tre på deltid – som driver BIM. Gruppen ska se till att BIM genomsyrar projektet, arbeta fram strategierna och se till att det som ska ske också sker. Detta görs till stor del med hjälp av identifierade champions ute i projektet.
​	– Vi är främst hjälp till självhjälp, säger Jenny Anghem, BIM Manager Skanska Healthcare AB. Alla är experter på sina arbetsområden och därför föredrar vi att lära våra kollegor hur de ska använda BIM-verktygen istället för att försöka göra deras arbete åt dem. När man gör sitt vanliga arbete plus något nytt som förenklar eller effektiviserar skapas förståelse för vad som sker och bättre tilltro till informationen. Det finns ett sug efter att jobba annorlunda eftersom man ser möjligheter i arbetet
med modellerna. Detta innebär att spridningen av BIM föder sig självt efter hand.
​	Tekniken utvecklas hela tiden och innebär sällan problem. Det allra viktigaste är att få kommunikationen människor emellan att fungera, att få dem att vilja göra små förändringar i sitt sätt att arbeta med nya verktyg.
​	 – Vi försöker förstå var och ens verklighet och verktyg och ser på vilket sätt BIM kan hjälpa till, säger John Fahlgren. 
​	Sedan projektet startade har merparten av BIM-arbetet legat inom designgruppen. Skanska valde att avstå från att ställa krav på att endast vissa programvaror skulle användas, utan har istället specificerat leveransformat. Konsulterna använder de programvaror de är vana att arbeta i. Viktigt har varit att få alla att acceptera att programvarorna är olika och jobba efter det, veta vilka riskerna är och hur det påverkar samarbetet.  
​	– Varje användare kan sin mjukvara men har kanske inte så mycket förståelse för den andra sidans behov av att leverera på ett bra sätt. Vi har förklarat vår synpunkt och försöker hitta framkomliga vägar som klarar att hantera de olika programmen. Det hade nog blivit större problem om man tvingat in folk i programvaror som de inte är vana vid, säger John Fahlgren.

![Bild 2](media/2.jpeg)

**BIM ska genomsyra allt i Nya Karolinska-projektet**

Modellerna används i dagsläget i samgranskningsmöten, som beslutsunderlag för att skapa bättre och renare projektering, och för att få ut mängder till olika intressenter. Planerare och inköpare har stor nytta av att använda modellerna när det gäller
att få fram information snabbt och ofta. 
​	BIM innefattas även i Skanskas kontraktsleverans mot kund. Kraven är tydliga – all tekniska dokumentation skall, direkt eller indirekt, vara kopplad till en objektsorienterad modell. För att genomföra detta har man valt att använda den för projektet framtagna märkningsstandard som nyckeln till att skapa en länk mellan verkligheten och modellen.
​	 – Kontraktsleveransens krav på informationsstruktur i modellen är en draghjälp för oss, säger John Fahlgren. Den har gjort det lättare att från början ställa krav och har skapat ordning och reda i modellerna, vilket skapat förutsättningar för implementering av flertalet olika BIM-applikationsområden.

INOM SKANSKA UK FINNS ERFARENHETER av stora sjukhusprojekt där BIM spelat en viktig roll. Där har man framförallt lyckats väl med användningen av BIM ute på byggplatsen. Med hjälp av Tablet-PCs har produktionscheferna haft tillgång till modellen, med ritningar och nyttig information länkad till sig, direkt ute på bygget. Det har sparat mycket tid och effektiviserat administrationen.
​	– Detta är ett nytt arbetssätt som vi kommer att använda oss av, säger John Fahlgren. Vi kommer även att titta på hur de implementerat BIM i organisationen och hur de kommunicerat för att få ett kraftfullt genomslag. Vad ser ni för eventuella svårigheter med BIM i projektet?
​	– Utmaningen för oss är att hitta enkla effektiva sätt att göra saker och ting på, säger Jenny Anghem. Eftersom vi inte vill vara en flaskhals, måste vi hitta arbetssätt som fungerar för gemene man.
​	– Att vara på plats hela tiden för dem som vi introducerat ett nytt arbetssätt till, säger John Fahlgren. Att kunna hjälpa dem så att de inte går tillbaka till att arbeta som tidigare. Det handlar mycket om att arbeta med och vara framgångsrik med de mjuka värdena.

NYA KAROLINSKA SOLNA ÄR ETT JÄTTEPROJEKT där farten är högt uppdriven och det gäller för BIM-gruppen att matcha det tempot. Gruppen försöker även hitta en lämplig nivå för vad som ska dokumenteras. Det är viktigt att dokumentera de stora stegen
på papper och få ut nya kunskaper till andra. Både Jenny Anghem och John Fahlgren poängterar att de försöker dela med sig så mycket som möjligt av sina erfarenheter
till konsulterna så att de får rätt information. Problemet är som vanligt att få tiden att räcka till och hitta bra sätt att sprida informationen på. Än så länge är projektet i sin linda. Det finns all anledning att så småningom återkomma till hur BIM-arbetet praktiskt går till i detta mycket stora projekt.